'use strict';
document.addEventListener('DOMContentLoaded', function() {
    clearButton.addEventListener('click', emptyInputField);
    printResult(randomizeButton, shuffle, resultContainer);
    textarea.addEventListener('keyup', autosize);
    textarea.addEventListener('keydown', showHideClearButton);
    userInput.onfocus = function() {
        if (customAlert.innerHTML !== '') {
            customAlert.classList.remove('active');
            customAlert.innerHTML = '';
        }
    } 
});

let userInput = document.getElementById('userInput');
let randomiserContainer = document.getElementById('modalRandomiser');
let resultContainer = document.getElementById('modalResult');
let randomizeButton = document.getElementById('btnRandomize');
let clearButton = document.querySelector('.clear-button');
let userInputValue = userInput.value;
const textarea = document.querySelector('textarea');
const customAlert = document.getElementById('alert');

let showHideCustomAlert = (customAlert) => {
    if (customAlert.innerHTML === '') {
        customAlert.classList.add('active');
        let msgElement = document.createElement('span');
        msgElement.innerHTML = 'Items are separated with comma or line break. Give it another try!';
        customAlert.appendChild(msgElement);    
    }
}
function autosize() {
    const el = this;
    let numberOfBreaks = (el.value.match(/\n/g)||[]).length;
    if (numberOfBreaks === 0) {
        el.style.height = "32px";
        return;
    }
    el.style.height = 'auto';
    el.style.height = (el.scrollHeight) + 'px';
}

let emptyInputField = (e) => {
    e.preventDefault();
    let userInput = document.getElementById('userInput');
    userInput.value = '';
    // TODO kan autosize köras ist?
    userInput.style.cssText = 'height:46px;';
    userInput.focus();
    return;
};

function showHideClearButton() {
    let clearButton = document.querySelector('.clear-button');
    const el = this;
    setTimeout(() => {
        el.value.length >= 1 ? clearButton.classList.add('active') : clearButton.classList.remove('active');
    }, 0);
}

let shuffle = (userInputValue) => {
    // TODO es6 and understandable :)
    for (let i = userInputValue.length -1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [userInputValue[i], userInputValue[j]] = [userInputValue[j], userInputValue[i]];
    }
    return userInputValue;
}

// TODO separate, its doing too much!
function printResult(randomizeButton, shuffle, resultContainer) {
    randomizeButton.addEventListener('click', function () {
        if (customAlert.innerHTML !== '') {
            customAlert.classList.remove('active');
            customAlert.innerHTML = '';
        }
        let items = document.getElementById('userInput').value;
        let resultList = document.getElementById('resultList');
        let resultHeading = document.getElementById('resultHeading');
        let array = items.split(/[,;\t\r\n]+/);
        // TODO use switch to catch stupid scenarios and let user know exactly why it fails.
        if (array.length === 1) {
            showHideCustomAlert(customAlert);
            return;
        }
        resultList.innerHTML = '';
        resultContainer.classList.remove('active');
        resultList.classList.remove('active');
        resultHeading.classList.remove('active');
        shuffle(array);
        array.forEach(function(item){
            if (item === " " || item === "") return;
            let theKid = document.createElement("li");
            theKid.innerHTML = item;
            resultList.appendChild(theKid);     
        });
        if (window.innerWidth >= 1245) {
            randomiserContainer.classList.add('scootchLeftForResult');
        }
        resultContainer.classList.add('active');
        setTimeout(() => {
            resultList.classList.add('active');
            resultHeading.classList.add('active');    
        }, 200);
 
    });
}